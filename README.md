# Codex Example Extension

This MediaWiki extension demonstrates how to use design tokens, components, and icons from the
[Codex design system](https://doc.wikimedia.org/codex/latest/).

## Resources

To learn more about Codex itself, visit the [Codex documentation website](https://doc.wikimedia.org/codex/latest/).

To learn more about using Codex inside MediaWiki, read on or visit https://www.mediawiki.org/wiki/Codex.

## Installation

Clone this extension into the `extensions/` directory in your local MediaWiki core repository.

Load the extension in LocalSettings.php:

```php
wfLoadExtension( 'CodexExample' );
```

Then visit Special:CodexExample to see Codex demos.

## Contents

This extension includes the following:

### Demo of Codex Vue components

The `ext.codexExample` module contains several Vue components demonstrating use of different Codex
Vue components. Check out the files located in `resources/ext.codexExample`.

Note that the Codex documentation site has code samples that show how to use each component. These
samples will not work as-is inside MediaWiki. You should review the [list of differences between Codex documentation and MediaWiki usage](https://www.mediawiki.org/wiki/Codex#Differences_between_Codex_documentation_and_MediaWiki_usage).

To learn more about the available Codex components, visit the [Components section](https://doc.wikimedia.org/codex/latest/components/overview.html)
of the docs site.

### Demo of Codex CSS-only components

The CodexExample special page contains several Codex CSS-only components: a set of static demos,
plus a CSS-only progress bar that displays before the Vue app loads. To see the code for these
components, check out:

- The mustache templates in `includes/templates`
- The styles that are loaded initially and apply CSS-only icons (see `resources/ext.codexExample.styles/styles.less`)
- The `codex-styles` ResourceLoader module is added in `includes/SpecialCodexExample.php`

To learn more about the available Codex components, visit the [Components section](https://doc.wikimedia.org/codex/latest/components/overview.html)
of the docs site and view the individual component demo pages. If there is a CSS-only version of
that component, documentation for the CSS-only version will be displayed at the end of the demo
page (e.g. https://doc.wikimedia.org/codex/latest/components/demos/button.html#css-only-version).

### Demo of Codex design tokens (and mixins)

Codex design tokens are used in several places in this codebase. In Less code, they are imported
via the MediaWiki skin variables. For examples, see `resources/ext.codexExample.styles/styles.less`
and the Vue files in `resources/ext.codexExample`.

Note that Codex mixins (like the CSS-only icon mixin) are also loaded via the skin variables.

### Demo of Codex icons

Codex icons are used in a few places in this codebase:
- Vue icons: `resources/ext.codexExample/components/ButtonDemo.vue` and `resources/ext.codexExample/components/DialogDemo.vue`
- CSS-only icons: `includes/templates/CssComponents.mustache` and `resources/ext.codexExample.styles/styles.less`
