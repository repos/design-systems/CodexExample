const { shallowMount } = require( '@vue/test-utils' );
const { CdxButton } = require( '@wikimedia/codex' );
const ButtonDemo = require( './ButtonDemo.vue' );

describe( 'Basic usage', () => {
	it( 'renders a Codex Button component', () => {
		const wrapper = shallowMount( ButtonDemo );
		expect( wrapper.findComponent( CdxButton ).exists() ).toBe( true );
	} );
} );
