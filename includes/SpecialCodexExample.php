<?php
/**
 * CodexExample Special page.
 *
 * @file
 */

namespace MediaWiki\Extension\CodexExample;

use TemplateParser;

class SpecialCodexExample extends \SpecialPage {

	/**
	 * @var TemplateParser
	 */
	private $templateParser;

	/**
	 * Initialize the special page.
	 */
	public function __construct() {
		parent::__construct( 'CodexExample' );

		$this->templateParser = new TemplateParser( __DIR__ . '/templates' );
	}

	/**
	 * Shows the page to the user.
	 * @param string $sub The subpage string argument (if any).
	 *  [[Special:HelloWorld/subpage]].
	 */
	public function execute( $sub ) {
		$out = $this->getOutput();
		$out->setPageTitleMsg( $this->msg( 'codexexample' ) );
		$out->addSubtitle( $this->msg( 'codexexample-subtitle' )->escaped() );

		// Add this extension's styles module, which contains render-blocking custom styles and
		// styles for several Codex CSS-only components. See extension.json.
		$out->addModuleStyles( [ 'ext.codexExample.styles' ] );

		// Add this extension's main module, which contains the Vue app and includes a subset of
		// Codex Vue components.
		$out->addModules( 'ext.codexExample' );

		// Add a template with some CSS-only components.
		$cssComponentsHtml = $this->templateParser->processTemplate(
			'CssComponents',
			$this->getCssComponentsData()
		);
		$out->addHTML( $cssComponentsHtml );

		// Add a template that contains the Vue root element, plus a CSS-only ProgressBar to show
		// that something is loading.
		$out->addHTML( $this->templateParser->processTemplate(
			'VueRoot',
			[ 'heading' => $this->msg( 'codexexample-app-heading' )->text() ]
		) );
	}

	private function getCssComponentsData() {
		return [
			'heading' => $this->msg( 'codexexample-css-heading' )->text(),
			'buttonHeading' => $this->msg( 'codexexample-css-button-heading' )->text(),
			'button1Label' => $this->msg( 'codexexample-css-button-1-label' )->text(),
			'button2Label' => $this->msg( 'codexexample-css-button-2-label' )->text(),
			'button3Label' => $this->msg( 'codexexample-css-button-3-label' )->text(),
			'cardHeading' => $this->msg( 'codexexample-css-card-heading' )->text(),
			'cardTitle' => $this->msg( 'codexexample-css-card-title' )->text(),
			'cardDescription' => $this->msg( 'codexexample-css-card-description' )->text(),
			'cardSupportingText' => $this->msg( 'codexexample-css-card-supporting-text' )->text(),
			'checkboxHeading' => $this->msg( 'codexexample-css-checkbox-heading' )->text(),
			'checkbox1Label' => $this->msg( 'codexexample-css-checkbox-1-label' )->text(),
			'checkbox2Label' => $this->msg( 'codexexample-css-checkbox-2-label' )->text(),
			'checkbox3Label' => $this->msg( 'codexexample-css-checkbox-3-label' )->text(),
		];
	}

	// phpcs:ignore
	protected function getGroupName() {
		return 'other';
	}
}
