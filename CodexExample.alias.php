<?php
/**
 * Aliases for Special:CodexExample
 * @file
 * @license GPL-2.0-or-later
 */

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'CodexExample' => [ 'CodexExample' ],
];
